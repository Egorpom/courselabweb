package ru.ssau.courselabweb.service;

import ru.ssau.courselabweb.Utils;
import ru.ssau.courselabweb.dao.SessionDAO;
import ru.ssau.courselabweb.entity.Session;

import java.sql.Connection;
import java.sql.SQLException;
import java.time.LocalDateTime;

public class SessionService {
    public static final SessionService INSTANCE = new SessionService();

    private SessionService() {
    }

    public void initTestData() {
        try (Connection connection = Utils.getConnection()) {
            for (int i = 0; i < 21; i++) {
                Session session = new Session();
                session.setAddress("Address" + i);
                session.setDatetime(LocalDateTime.now());
                SessionDAO.INSTANCE.save(connection, session);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
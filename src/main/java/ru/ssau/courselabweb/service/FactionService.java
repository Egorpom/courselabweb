package ru.ssau.courselabweb.service;

import ru.ssau.courselabweb.Utils;
import ru.ssau.courselabweb.dao.FactionDAO;
import ru.ssau.courselabweb.entity.Faction;

import java.sql.Connection;
import java.sql.SQLException;

public class FactionService {
    public static final FactionService INSTANCE = new FactionService();

    private FactionService() {
    }

    public void initTestData() {
        try (Connection connection = Utils.getConnection()) {
            for (int i = 0; i < 21; i++) {
                Faction faction = new Faction();
                faction.setTitle("Faction" + i);
                faction.setAddress("Address" + i);
                faction.setPhoneNumber("PhoneNumber" + i);
                FactionDAO.INSTANCE.save(connection, faction);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
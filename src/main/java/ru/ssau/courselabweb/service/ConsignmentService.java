package ru.ssau.courselabweb.service;

import ru.ssau.courselabweb.Utils;
import ru.ssau.courselabweb.dao.ConsignmentDAO;
import ru.ssau.courselabweb.entity.Consignment;

import java.sql.Connection;
import java.sql.SQLException;

public class ConsignmentService {
    public static final ConsignmentService INSTANCE = new ConsignmentService();

    private ConsignmentService() {
    }

    public void initTestData() {
        try (Connection connection = Utils.getConnection()) {
            for (int i = 0; i < 21; i++) {
                Consignment consignment = new Consignment();
                consignment.setTitle("Faction" + i);
                consignment.setPhoneNumber("PhoneNumber" + i);
                consignment.setAddress("Address" + i);
                consignment.setFactionId(i + 1);
                ConsignmentDAO.INSTANCE.save(connection, consignment);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
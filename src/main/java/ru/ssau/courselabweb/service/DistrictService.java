package ru.ssau.courselabweb.service;

import ru.ssau.courselabweb.Utils;
import ru.ssau.courselabweb.dao.DistrictDAO;
import ru.ssau.courselabweb.entity.District;

import java.sql.Connection;
import java.sql.SQLException;

public class DistrictService {
    public static final DistrictService INSTANCE = new DistrictService();

    private DistrictService() {
    }

    public void initTestData() {
        try (Connection connection = Utils.getConnection()) {
            for (int i = 0; i < 21; i++) {
                District district = new District();
                district.setName("District" + i);
                district.setAbbreviation("D" + i);
                DistrictDAO.INSTANCE.save(connection, district);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
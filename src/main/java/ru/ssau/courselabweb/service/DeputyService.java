package ru.ssau.courselabweb.service;

import ru.ssau.courselabweb.Utils;
import ru.ssau.courselabweb.dao.DeputyDAO;
import ru.ssau.courselabweb.entity.Deputy;

import java.sql.Connection;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.concurrent.ThreadLocalRandom;

public class DeputyService {
    public static final DeputyService INSTANCE = new DeputyService();

    private DeputyService() {
    }

    public void initTestData() {
        try (Connection connection = Utils.getConnection()) {
            for (int i = 0; i < 21; i++) {
                Deputy deputy = new Deputy();
                deputy.setPassportId(generatePassportId());
                deputy.setFullName("Deputy" + i);
                deputy.setBirthday(LocalDate.now());
                deputy.setPosition("Position" + i);
                deputy.setSalary(ThreadLocalRandom.current().nextInt(12000, 100001));
                deputy.setConsignmentId(i + 1);
                deputy.setDistrictId(i + 1);
                DeputyDAO.INSTANCE.save(connection, deputy);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private static String generatePassportId() {
        StringBuilder passportId = new StringBuilder();
        for (int i = 0; i < 10; i++) {
            passportId.append(ThreadLocalRandom.current().nextInt(0, 10));
        }
        return passportId.toString();
    }
}
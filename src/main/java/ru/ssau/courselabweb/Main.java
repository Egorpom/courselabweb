package ru.ssau.courselabweb;

import ru.ssau.courselabweb.dao.DeputyDAO;
import ru.ssau.courselabweb.dao.SessionDAO;
import ru.ssau.courselabweb.entity.Deputy;
import ru.ssau.courselabweb.entity.Session;
import ru.ssau.courselabweb.service.*;

import java.sql.Connection;
import java.util.Arrays;

public class Main {
    public static void main(String[] args) throws Exception {
        DistrictService.INSTANCE.initTestData();
        SessionService.INSTANCE.initTestData();
        FactionService.INSTANCE.initTestData();
        ConsignmentService.INSTANCE.initTestData();
        DeputyService.INSTANCE.initTestData();
        addParticipantsData();
    }

    private static void addParticipantsData() throws Exception {
        try (Connection connection = Utils.getConnection()) {
            Session session = new Session();
            session.setId(10);
            SessionDAO.INSTANCE.addDeputiesToSession(connection, session, Arrays.asList(2, 4, 8));
            Deputy deputy = new Deputy();
            deputy.setId(20);
            DeputyDAO.INSTANCE.addSessionsToDeputy(connection, deputy, Arrays.asList(1, 3, 5));
        }
    }
}
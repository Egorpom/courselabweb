package ru.ssau.courselabweb.dao;

import ru.ssau.courselabweb.Utils;
import ru.ssau.courselabweb.entity.Session;

import java.sql.*;
import java.util.Collections;
import java.util.List;

public class SessionDAO extends DAO<Session> {
    public static final SessionDAO INSTANCE = new SessionDAO();

    private SessionDAO() {
        super("session", "datetime", "address");
    }

    @Override
    protected Session resultSetToEntity(ResultSet set) throws SQLException {
        Session entity = new Session();
        entity.setId(set.getInt(1));
        entity.setDatetime(set.getTimestamp(2).toLocalDateTime());
        entity.setAddress(set.getString(3));
        return entity;
    }

    @Override
    protected void entityToStatement(Session entity, PreparedStatement statement) throws SQLException {
        statement.setTimestamp(1, Timestamp.valueOf(entity.getDatetime()));
        statement.setString(2, entity.getAddress());
    }

    public void addDeputyToSession(Session entity, Integer deputy) throws SQLException {
        try (Connection connection = Utils.getConnection()) {
            addDeputyToSession(connection, entity, deputy);
        }
    }

    public void addDeputyToSession(Connection connection, Session entity, Integer deputy) throws SQLException {
        addDeputiesToSession(connection, entity, Collections.singletonList(deputy));
    }

    public void addDeputiesToSession(Session entity, List<Integer> deputies) throws SQLException {
        try (Connection connection = Utils.getConnection()) {
            addDeputiesToSession(connection, entity, deputies);
        }
    }

    public void addDeputiesToSession(Connection connection, Session entity, List<Integer> deputies) throws SQLException {
        int entityId = entity.getId();
        try (PreparedStatement statement = connection.prepareStatement(Utils.buildQueryForParticipant(deputies.size()))) {
            for (int i = 0; i < deputies.size(); i++) {
                statement.setInt(i * 2 + 1, entityId);
                statement.setInt(i * 2 + 2, deputies.get(i));
            }
            statement.executeUpdate();
        }
    }
}
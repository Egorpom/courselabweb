package ru.ssau.courselabweb.dao;

import ru.ssau.courselabweb.Utils;
import ru.ssau.courselabweb.entity.Entity;

import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public abstract class DAO<T extends Entity> {
    protected String tableName;
    protected List<String> columnNames;

    protected DAO(String tableName, String... columnNames) {
        this.tableName = tableName;
        this.columnNames = new ArrayList<>(Arrays.asList(columnNames));
    }

    public T get(int id) throws SQLException {
        T entity;
        try (Connection connection = Utils.getConnection()) {
            entity = get(connection, id);
        }
        return entity;
    }

    public T get(Connection connection, int id) throws SQLException {
        T entity = null;
        String sql = "SELECT " + getColumnNames() + " FROM " + tableName + " WHERE id = ?";
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setInt(1, id);
            try (ResultSet set = statement.executeQuery()) {
                if (set.next()) {
                    entity = resultSetToEntity(set);
                }
            }
        }
        return entity;
    }

    public List<T> getAll() throws SQLException {
        List<T> entities;
        try (Connection connection = Utils.getConnection()) {
            entities = getAll(connection);
        }
        return entities;
    }

    public List<T> getAll(Connection connection) throws SQLException {
        List<T> entities = new ArrayList<>();
        try (Statement statement = connection.createStatement()) {
            try (ResultSet set = statement.executeQuery("SELECT " + getColumnNames() + " FROM " + tableName)) {
                while (set.next()) {
                    entities.add(resultSetToEntity(set));
                }
            }
        }
        Collections.sort(entities);
        return entities;
    }

    public List<T> getByIntColumn(String columnName, Object value) throws SQLException {
        List<T> entities;
        try (Connection connection = Utils.getConnection()) {
            entities = getByIntColumn(connection, columnName, value);
        }
        return entities;
    }

    public List<T> getByIntColumn(Connection connection, String columnName, Object value) throws SQLException {
        List<T> entities = new ArrayList<>();
        String sql = "SELECT " + getColumnNames() + " FROM " + tableName + " WHERE " + columnName + " = ?";
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setObject(1, value);
            try (ResultSet set = statement.executeQuery()) {
                while (set.next()) {
                    entities.add(resultSetToEntity(set));
                }
            }
        }
        return entities;
    }

    public int save(T entity) throws SQLException {
        try (Connection connection = Utils.getConnection()) {
            return save(connection, entity);
        }
    }

    public int save(Connection connection, T entity) throws SQLException {
        StringBuilder sql = new StringBuilder("INSERT INTO ")
                .append(tableName)
                .append("(")
                .append(String.join(",", columnNames))
                .append(") VALUES(");
        for (int i = 0; i < columnNames.size(); i++) {
            if (i != 0) {
                sql.append(",");
            }
            sql.append("?");
        }
        sql.append(") RETURNING id");
        try (PreparedStatement statement = connection.prepareStatement(sql.toString())) {
            entityToStatement(entity, statement);
            try (ResultSet set = statement.executeQuery()) {
                if (set.next()) {
                    return set.getInt(1);
                }
            }
        }
        return -1;
    }

    public void update(T entity) throws SQLException {
        try (Connection connection = Utils.getConnection()) {
            update(connection, entity);
        }
    }

    public void update(Connection connection, T entity) throws SQLException {
        StringBuilder builder = new StringBuilder();
        builder.append("UPDATE ").append(tableName).append(" SET ");
        for (int i = 0; i < columnNames.size(); i++) {
            if (i != 0) {
                builder.append(",");
            }
            builder.append(columnNames.get(i)).append(" = ").append("?");
        }
        builder.append(" WHERE id = ?");
        try (PreparedStatement statement = connection.prepareStatement(builder.toString())) {
            entityToStatement(entity, statement);
            statement.setInt(columnNames.size() + 1, entity.getId());
            statement.executeUpdate();
        }
    }

    public void delete(T entity) throws SQLException {
        try (Connection connection = Utils.getConnection()) {
            delete(connection, entity);
        }
    }

    public void delete(Connection connection, T entity) throws SQLException {
        try (PreparedStatement statement = connection.prepareStatement("DELETE FROM " + tableName + " WHERE id = ?")) {
            statement.setInt(1, entity.getId());
            statement.executeUpdate();
        }
    }

    protected String getColumnNames() {
        return "id," + String.join(",", columnNames);
    }

    abstract protected T resultSetToEntity(ResultSet set) throws SQLException;

    abstract protected void entityToStatement(T entity, PreparedStatement statement) throws SQLException;
}
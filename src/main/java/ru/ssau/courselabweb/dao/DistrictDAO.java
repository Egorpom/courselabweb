package ru.ssau.courselabweb.dao;

import ru.ssau.courselabweb.entity.District;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class DistrictDAO extends DAO<District> {
    public static final DistrictDAO INSTANCE = new DistrictDAO();

    private DistrictDAO() {
        super("district", "name", "abbreviation");
    }

    @Override
    protected District resultSetToEntity(ResultSet set) throws SQLException {
        District entity = new District();
        entity.setId(set.getInt(1));
        entity.setName(set.getString(2));
        entity.setAbbreviation(set.getString(3));
        return entity;
    }

    @Override
    protected void entityToStatement(District entity, PreparedStatement statement) throws SQLException {
        statement.setString(1, entity.getName());
        statement.setString(2, entity.getAbbreviation());
    }
}
package ru.ssau.courselabweb.dao;

import ru.ssau.courselabweb.Utils;
import ru.ssau.courselabweb.entity.Deputy;

import java.sql.*;
import java.util.Collections;
import java.util.List;

public class DeputyDAO extends DAO<Deputy> {
    public static final DeputyDAO INSTANCE = new DeputyDAO();

    private DeputyDAO() {
        super("deputy", "passport_id", "full_name", "birthday", "position", "salary", "consignment_id", "district_id");
    }

    @Override
    protected Deputy resultSetToEntity(ResultSet set) throws SQLException {
        Deputy entity = new Deputy();
        entity.setId(set.getInt(1));
        entity.setPassportId(set.getString(2));
        entity.setFullName(set.getString(3));
        entity.setBirthday(set.getDate(4).toLocalDate());
        entity.setPosition(set.getString(5));
        entity.setSalary(set.getInt(6));
        entity.setConsignmentId((Integer) set.getObject(7));
        entity.setDistrictId((Integer) set.getObject(8));
        return entity;
    }

    @Override
    protected void entityToStatement(Deputy entity, PreparedStatement statement) throws SQLException {
        statement.setString(1, entity.getPassportId());
        statement.setString(2, entity.getFullName());
        statement.setDate(3, Date.valueOf(entity.getBirthday()));
        statement.setString(4, entity.getPosition());
        statement.setObject(5, entity.getSalary());
        statement.setObject(6, entity.getConsignmentId());
        statement.setObject(7, entity.getDistrictId());
    }

    public void addSessionToDeputy(Deputy entity, Integer session) throws SQLException {
        try (Connection connection = Utils.getConnection()) {
            addSessionToDeputy(connection, entity, session);
        }
    }

    public void addSessionToDeputy(Connection connection, Deputy entity, Integer session) throws SQLException {
        addSessionsToDeputy(connection, entity, Collections.singletonList(session));
    }

    public void addSessionsToDeputy(Deputy entity, List<Integer> sessions) throws SQLException {
        try (Connection connection = Utils.getConnection()) {
            addSessionsToDeputy(connection, entity, sessions);
        }
    }

    public void addSessionsToDeputy(Connection connection, Deputy entity, List<Integer> sessions) throws SQLException {
        int entityId = entity.getId();
        try (PreparedStatement statement = connection.prepareStatement(Utils.buildQueryForParticipant(sessions.size()))) {
            for (int i = 0; i < sessions.size(); i++) {
                statement.setInt(i * 2 + 1, sessions.get(i));
                statement.setInt(i * 2 + 2, entityId);
            }
            statement.executeUpdate();
        }
    }
}

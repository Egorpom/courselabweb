package ru.ssau.courselabweb.dao;

import ru.ssau.courselabweb.entity.Consignment;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class ConsignmentDAO extends DAO<Consignment> {
    public static final ConsignmentDAO INSTANCE = new ConsignmentDAO();

    private ConsignmentDAO() {
        super("consignment", "title", "phone_number", "address", "faction_id", "leader_id");
    }

    @Override
    protected Consignment resultSetToEntity(ResultSet set) throws SQLException {
        Consignment entity = new Consignment();
        entity.setId(set.getInt(1));
        entity.setTitle(set.getString(2));
        entity.setPhoneNumber(set.getString(3));
        entity.setAddress(set.getString(4));
        entity.setFactionId((Integer) set.getObject(5));
        entity.setLeaderId((Integer) set.getObject(6));
        return entity;
    }

    @Override
    protected void entityToStatement(Consignment entity, PreparedStatement statement) throws SQLException {
        statement.setString(1, entity.getTitle());
        statement.setString(2, entity.getPhoneNumber());
        statement.setString(3, entity.getAddress());
        statement.setObject(4, entity.getFactionId());
        statement.setObject(5, entity.getLeaderId());
    }
}
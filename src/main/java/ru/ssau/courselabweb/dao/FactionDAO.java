package ru.ssau.courselabweb.dao;

import ru.ssau.courselabweb.entity.Faction;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class FactionDAO extends DAO<Faction> {
    public static final FactionDAO INSTANCE = new FactionDAO();

    private FactionDAO() {
        super("faction", "title", "address", "phone_number", "leader_id");
    }

    @Override
    protected Faction resultSetToEntity(ResultSet set) throws SQLException {
        Faction entity = new Faction();
        entity.setId(set.getInt(1));
        entity.setTitle(set.getString(2));
        entity.setAddress(set.getString(3));
        entity.setPhoneNumber(set.getString(4));
        entity.setLeaderId((Integer) set.getObject(5));
        return entity;
    }

    @Override
    protected void entityToStatement(Faction entity, PreparedStatement statement) throws SQLException {
        statement.setString(1, entity.getTitle());
        statement.setString(2, entity.getAddress());
        statement.setString(3, entity.getPhoneNumber());
        statement.setObject(4, entity.getLeaderId());
    }
}
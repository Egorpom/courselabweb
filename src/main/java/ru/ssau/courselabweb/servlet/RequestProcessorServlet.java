package ru.ssau.courselabweb.servlet;

import ru.ssau.courselabweb.Utils;
import ru.ssau.courselabweb.dao.*;
import ru.ssau.courselabweb.entity.*;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

@WebServlet(name = "RequestProcessorServlet", urlPatterns = "/rps")
public class RequestProcessorServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        processRequest(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        processRequest(req, resp);
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        processRequest(req, resp);
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        processRequest(req, resp);
    }

    @SuppressWarnings("unchecked")
    private static void processRequest(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setCharacterEncoding("UTF-8");
        String type = request.getHeader("Entity-Type");
        if (type == null) {
            return;
        }
        Class<? extends Entity> entityClass;
        DAO dao;
        switch (type.toLowerCase()) {
            case "district":
                entityClass = District.class;
                dao = DistrictDAO.INSTANCE;
                break;
            case "deputy":
                entityClass = Deputy.class;
                dao = DeputyDAO.INSTANCE;
                break;
            case "consignment":
                entityClass = Consignment.class;
                dao = ConsignmentDAO.INSTANCE;
                break;
            case "faction":
                entityClass = Faction.class;
                dao = FactionDAO.INSTANCE;
                break;
            case "session":
                entityClass = Session.class;
                dao = SessionDAO.INSTANCE;
                break;
            default:
                response.sendError(HttpServletResponse.SC_BAD_REQUEST);
                return;
        }
        Entity entity = Utils.GSON.fromJson(request.getReader(), entityClass);
        try {
            PrintWriter writer = response.getWriter();
            switch (request.getMethod()) {
                case "POST":
                    int id = dao.save(entity);
                    writer.print(id);
                    break;
                case "PUT":
                    dao.update(entity);
                    break;
                case "DELETE":
                    dao.delete(entity);
                    break;
                case "GET":
                    Utils.GSON.toJson(dao.getAll(), writer);
                    break;
            }
            response.setStatus(HttpServletResponse.SC_OK);
        } catch (SQLException ex) {
            ex.printStackTrace();
            response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        }
    }

}
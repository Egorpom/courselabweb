package ru.ssau.courselabweb.entity;

import lombok.Data;

@Data
public class Faction implements Entity {
    private Integer id;
    private String title;
    private String address;
    private String phoneNumber;
    private Integer leaderId;
}
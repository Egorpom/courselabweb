package ru.ssau.courselabweb.entity;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class Session implements Entity {
    private Integer id;
    private LocalDateTime datetime;
    private String address;
}
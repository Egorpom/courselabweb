package ru.ssau.courselabweb.entity;

import lombok.Data;

import java.time.LocalDate;

@Data
public class Deputy implements Entity {
    private Integer id;
    private String passportId;
    private String fullName;
    private LocalDate birthday;
    private String position;
    private Integer salary;
    private Integer consignmentId;
    private Integer districtId;
}
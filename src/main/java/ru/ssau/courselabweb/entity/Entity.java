package ru.ssau.courselabweb.entity;

import java.io.Serializable;

public interface Entity extends Comparable<Entity>, Serializable {
    Integer getId();

    @Override
    default int compareTo(Entity o) {
        return Integer.compare(this.getId(), o.getId());
    }
}
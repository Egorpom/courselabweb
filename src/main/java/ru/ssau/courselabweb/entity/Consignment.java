package ru.ssau.courselabweb.entity;

import lombok.Data;

@Data
public class Consignment implements Entity {
    private Integer id;
    private String title;
    private String phoneNumber;
    private String address;
    private Integer leaderId;
    private Integer factionId;
}
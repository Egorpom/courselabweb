package ru.ssau.courselabweb.entity;

import lombok.Data;

@Data
public class District implements Entity {
    private Integer id;
    private String name;
    private String abbreviation;
}
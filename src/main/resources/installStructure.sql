CREATE SEQUENCE IF NOT EXISTS consignment_id_seq INCREMENT BY 1;

CREATE SEQUENCE IF NOT EXISTS deputy_id_seq INCREMENT BY 1;

CREATE SEQUENCE IF NOT EXISTS district_id_seq INCREMENT BY 1;

CREATE SEQUENCE IF NOT EXISTS faction_id_seq INCREMENT BY 1;

CREATE SEQUENCE IF NOT EXISTS session_id_seq INCREMENT BY 1;

CREATE TABLE IF NOT EXISTS consignment (
  id           INTEGER DEFAULT nextval('consignment_id_seq') NOT NULL CONSTRAINT consignment_pkey PRIMARY KEY,
  address      VARCHAR(255),
  phone_number VARCHAR(255),
  title        VARCHAR(255)                                  NOT NULL,
  faction_id   INTEGER                                       NOT NULL,
  leader_id    INTEGER CONSTRAINT consignment_leader_id_key UNIQUE
);

CREATE INDEX IF NOT EXISTS consignment_faction_id_idx ON consignment (faction_id);

CREATE TABLE IF NOT EXISTS district (
  id           INTEGER DEFAULT nextval('district_id_seq') NOT NULL CONSTRAINT district_pkey PRIMARY KEY,
  abbreviation VARCHAR(255),
  name         VARCHAR(255)                               NOT NULL
);

CREATE TABLE IF NOT EXISTS deputy (
  id             INTEGER DEFAULT nextval('deputy_id_seq') NOT NULL CONSTRAINT deputy_pkey PRIMARY KEY,
  birthday       DATE,
  full_name      VARCHAR(255)                             NOT NULL,
  passport_id    VARCHAR(255)                             NOT NULL,
  position       VARCHAR(255),
  salary         INTEGER,
  consignment_id INTEGER                                  NOT NULL CONSTRAINT deputy_consignment_id_fkey REFERENCES consignment,
  district_id    INTEGER                                  NOT NULL CONSTRAINT deputy_district_id_fkey REFERENCES district
);

ALTER TABLE consignment
  ADD CONSTRAINT consignment_leader_id_fkey FOREIGN KEY (leader_id) REFERENCES deputy;

CREATE INDEX IF NOT EXISTS deputy_consignment_id_idx ON deputy (consignment_id);

CREATE INDEX IF NOT EXISTS deputy_district_id_idx ON deputy (district_id);

CREATE TABLE IF NOT EXISTS faction (
  id           INTEGER DEFAULT nextval('faction_id_seq') NOT NULL CONSTRAINT faction_pkey PRIMARY KEY,
  address      VARCHAR(255),
  phone_number VARCHAR(255),
  title        VARCHAR(255)                              NOT NULL,
  leader_id    INTEGER CONSTRAINT faction_leader_id_key UNIQUE CONSTRAINT faction_leader_id_fkey REFERENCES consignment
);

ALTER TABLE consignment
  ADD CONSTRAINT consignment_faction_id_fkey FOREIGN KEY (faction_id) REFERENCES faction;

CREATE TABLE IF NOT EXISTS session (
  id       INTEGER DEFAULT nextval('session_id_seq') NOT NULL CONSTRAINT session_pkey PRIMARY KEY,
  address  VARCHAR(255),
  datetime TIMESTAMP                                 NOT NULL
);

CREATE TABLE IF NOT EXISTS session_participant (
  session_id INTEGER NOT NULL CONSTRAINT session_participant_session_id_fkey REFERENCES session,
  deputy_id  INTEGER NOT NULL CONSTRAINT session_participant_deputy_id_fkey REFERENCES deputy,
  CONSTRAINT session_participant_pkey PRIMARY KEY (session_id, deputy_id)
);

CREATE INDEX IF NOT EXISTS session_participant_session_id_idx ON session_participant (session_id);

CREATE INDEX IF NOT EXISTS session_participant_deputy_id_idx ON session_participant (deputy_id);
function loadData(tabButton) {
    var table = $('#' + $(tabButton).attr('href').substring(1));
    if (!table.prop('loaded')) {
        table.prop('loaded', true);
        $.ajax({
            url: '/rps',
            method: 'GET',
            headers: {'Entity-Type': table.attr('id')},
            success: function (data) {
                var entities = JSON.parse(data);
                var defaultRow = table.find('#default');
                var lastRow = defaultRow;
                var newRow;
                entities.forEach(function (entity) {
                    newRow = defaultRow.clone();
                    lastRow.after(newRow);
                    lastRow = newRow;
                    newRow.find('input').each(function () {
                        $(this).val(entity[$(this).attr('id')]);
                    });
                    newRow.removeAttr('id');
                    newRow.show();
                });
            }
        });
    }
}

/**
 * Вызывается при клике на иконку сохранения.
 * Отправляет новый/измененный entity на сервер.
 * При успешном сохранении присваивает ID новому entity.
 * В противном случае восстанавливает предыдущие значения/удаляет строку, если entity ещё не был создан.
 * @param item - иконка сохранения
 */
function save(item) {
    var parent = getParent(item);
    var entity = {};
    parent.find('input').each(function () {
        var val = $(this).val();
        if (val === "") {
            val = null;
        }
        entity[$(this).attr('id')] = val;
    });
    var created = entity["id"] > 0;
    var method = created ? "PUT" : "POST";
    toggleIcons(parent);
    switchInputs(parent, true);
    $.ajax({
        url: '/rps',
        method: method,
        headers: {'Entity-Type': parent.closest('div').attr('id')},
        data: JSON.stringify(entity),
        success: function (data) {
            if (!created) {
                parent.find("#id").val(data);
            }
        },
        error: function (data) {
            if (created) {
                restoreInputs(parent);
            } else {
                parent.remove();
            }
        }
    });
}

/**
 * Вызывается при клике на иконку редактирования и при добавлении нового элемента в таблицу.
 * Подготавливает строку таблицы к редактированию.
 * @param item - иконка редактирования
 */
function edit(item) {
    var parent = getParent(item);
    toggleIcons(parent);
    backupInputs(parent);
    switchInputs(parent, false);
}

/**
 * Вызывается при клике на иконку отмены при редактировании.
 * Восстанавливает значения полей.
 * @param item - иконка отмены
 */
function cancel(item) {
    var parent = getParent(item);
    if (parent.find('#id').val() >= 0) {
        toggleIcons(parent);
        restoreInputs(parent);
        switchInputs(parent, true);
    } else {
        parent.remove();
    }
}

/**
 * Вызывается при клике на иконку удаления.
 * Запрашивает подтверждение перед удалением.
 * Если подтверждение получено, то будет отправлен запрос на удаление и строка в таблице будет скрыта.
 * Если удаление прошло успешно, то срока удалится совсем.
 * В противном случае строка будет восстановлена и выведется ошибка.
 * @param item - иконка удаления
 */
function deleteEntity(item) {
    if (confirm("Are you want delete this entity?")) {
        var parent = getParent(item);
        parent.hide();
        var entity = {};
        entity['id'] = parent.find('#id').val();
        $.ajax({
            url: '/rps',
            method: 'DELETE',
            headers: {'Entity-Type': parent.closest('div').attr('id')},
            data: JSON.stringify(entity),
            success: function (data) {
                parent.remove();
            },
            error: function (data) {
                parent.show();
            }
        });
    }
}

/**
 * Вызывается при нажатии на кнопку добавления.
 * Клонирует скрытую строку и добавляет новую с id=-1.
 */
function add() {
        var row = $("#default");
        var newRow = row.clone();
        newRow.find("#id").val(-1);
        newRow.find("#delete").hide();
        row.closest('table').find('tr:last').after(newRow);
        newRow.show();
        edit(newRow);
}

/**
 *
 * @param item - иконка удаления/отмены/редактирования/сохранения
 * @returns {HTMLTableRowElement | null | Element | null | any | null | jQuery} - возвращает <tr> строки, в которой находится item.
 */
function getParent(item) {
    return $(item).closest('tr');
}

/**
 * Переключает отображение иконок в режим редактирования и наоборот.
 * @param parent - <tr>
 */
function toggleIcons(parent) {
    parent.find("#save").toggle();
    parent.find("#cancel").toggle();
    parent.find("#edit").toggle();
}

/**
 * Сохраняет текущее значение полей в строке в атрибут data-backup.
 * @param parent - <tr>
 */
function backupInputs(parent) {
    parent.find('input').each(function () {
        $(this).data("backup", $(this).val());
    });
}

/**
 * В зависимости от value разрешает/запрещает редактирование полей строки.
 * @param parent - <tr>
 * @param value - boolean
 */
function switchInputs(parent, value) {
    parent.find('input').each(function () {
        if ($(this).attr('id') !== 'id') {
            $(this).prop("readonly", value);
        }
    });
}

/**
 * Восстанавливает значение полей в строке из атрибута data-backup.
 * @param parent - <tr>
 */
function restoreInputs(parent) {
    var item;
    parent.find('input').each(function () {
        $(this).val($(this).data("backup"));
    });
}
<html>
<head>
    <script src="webjars/jquery/3.3.1-2/jquery.min.js"></script>
    <script src="webjars/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <script src="js/script.js"></script>
    <link rel="stylesheet" type="text/css" href="webjars/bootstrap/4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="webjars/font-awesome/5.7.2/css/all.min.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <title>Meeting Manager</title>
</head>
<body>
<h1 class="mm-title">Meeting Manager</h1>
<ul class="nav nav-tabs" role="tablist">
    <li class="nav-item">
        <a class="nav-link active" href="#start" role="tab" data-toggle="tab">Start</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="#district" role="tab" data-toggle="tab" onclick="loadData(this)">Districts</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="#deputy" role="tab" data-toggle="tab" onclick="loadData(this)">Deputies</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="#faction" role="tab" data-toggle="tab" onclick="loadData(this)">Factions</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="#consignment" role="tab" data-toggle="tab" onclick="loadData(this)">Consignments</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="#session" role="tab" data-toggle="tab" onclick="loadData(this)">Sessions</a>
    </li>
</ul>
<div class="tab-content">
    <div role="tabpanel" class="tab-pane fade active show" id="start">
        <h2 class="welcome">Welcome to Meeting Manager</h2>
    </div>
    <div role="tabpanel" class="tab-pane fade" id="district">
        <table class="table table-bordered table-hover text-center">
            <thead class="thead-dark">
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Abbreviation</th>
                <th>Actions</th>
            </tr>
            </thead>
            <tbody>
            <tr id="default" style="display: none">
                <td><input id="id" class="itemInTable" required readonly></td>
                <td><input id="name" class="itemInTable" readonly></td>
                <td><input id="abbreviation" class="itemInTable" readonly></td>
                <%@include file="editColumn.html" %>
            </tr>
            </tbody>
        </table>
        <p align="center">
            <button type="button" class="btn btn-primary" onclick="add()">Add District</button>
        </p>
    </div>
    <div role="tabpanel" class="tab-pane fade" id="deputy">
        <table class="table table-bordered table-hover text-center">
            <thead class="thead-dark">
            <tr>
                <th>ID</th>
                <th>Passport ID</th>
                <th>Full Name</th>
                <th>Birthday</th>
                <th>Position</th>
                <th>Salary</th>
                <th>Consignment ID</th>
                <th>District ID</th>
                <th>Actions</th>
            </tr>
            </thead>
            <tbody>
            <tr id="default" style="display: none">
                <td><input id="id" class="itemInTable" required readonly></td>
                <td><input id="passportId" class="itemInTable" readonly></td>
                <td><input id="fullName" class="itemInTable" readonly></td>
                <td><input id="birthday" type="date" class="itemInTable" readonly></td>
                <td><input id="position" class="itemInTable" readonly></td>
                <td><input id="salary" class="itemInTable" readonly></td>
                <td><input id="consignmentId" class="itemInTable" readonly></td>
                <td><input id="districtId" class="itemInTable" readonly></td>
                <%@include file="editColumn.html" %>
            </tr>
            </tbody>
        </table>
        <p align="center">
            <button type="button" class="btn btn-primary" onclick="add()">Add Deputy</button>
        </p>
    </div>
    <div role="tabpanel" class="tab-pane fade" id="faction">
        <table class="table table-bordered table-hover text-center">
            <thead class="thead-dark">
            <tr>
                <th>ID</th>
                <th>Title</th>
                <th>Address</th>
                <th>Phone Number</th>
                <th>Leader ID</th>
                <th>Actions</th>
            </tr>
            </thead>
            <tbody>
            <tr id="default" style="display: none">
                <td><input id="id" class="itemInTable" readonly></td>
                <td><input id="title" class="itemInTable" readonly></td>
                <td><input id="address" class="itemInTable" readonly></td>
                <td><input id="phoneNumber" class="itemInTable" readonly></td>
                <td><input id="leaderId" class="itemInTable" readonly></td>
                <%@include file="editColumn.html" %>
            </tr>
            </tbody>
        </table>
        <p align="center">
            <button type="button" class="btn btn-primary" onclick="add()">Add Faction</button>
        </p>
    </div>
    <div role="tabpanel" class="tab-pane fade" id="consignment">
        <table class="table table-bordered table-hover text-center">
            <thead class="thead-dark">
            <tr>
                <th>ID</th>
                <th>Title</th>
                <th>Phone Number</th>
                <th>Address</th>
                <th>Leader ID</th>
                <th>Faction ID</th>
                <th>Actions</th>
            </tr>
            </thead>
            <tbody>
            <tr id="default" style="display: none">
                <td><input id="id" class="itemInTable" required readonly></td>
                <td><input id="title" class="itemInTable" readonly></td>
                <td><input id="phoneNumber" class="itemInTable" readonly></td>
                <td><input id="address" class="itemInTable" readonly></td>
                <td><input id="leaderId" class="itemInTable" readonly></td>
                <td><input id="factionId" class="itemInTable" readonly></td>
                <%@include file="editColumn.html" %>
            </tr>
            </tbody>
        </table>
        <p align="center">
            <button type="button" class="btn btn-primary" onclick="add()">Add Consignment</button>
        </p>
    </div>
    <div role="tabpanel" class="tab-pane fade" id="session">
        <table class="table table-bordered table-hover text-center">
            <thead class="thead-dark">
            <tr>
                <th>ID</th>
                <th>Date & Time</th>
                <th>Address</th>
                <th>Actions</th>
            </tr>
            </thead>
            <tbody>
            <tr id="default" style="display: none">
                <td><input id="id" class="itemInTable" required readonly></td>
                <td><input id="datetime" type="datetime-local" class="itemInTable" readonly></td>
                <td><input id="address" class="itemInTable" readonly></td>
                <%@include file="editColumn.html" %>
            </tr>
            </tbody>
        </table>
        <p align="center">
            <button type="button" class="btn btn-primary" onclick="add()">Add Session</button>
        </p>
    </div>
</div>
</body>
</html>